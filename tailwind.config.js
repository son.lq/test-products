/** @type {import('tailwindcss').Config} */
const colors = require('tailwindcss/colors')

module.exports = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx}',
    './components/**/*.{js,ts,jsx,tsx}'
  ],
  darkMode: 'class',
  theme: {
    extend: {
      colors: {
        dark: {
          DEFAULT: '#13141f',
          50: '#9AA1B7',
          100: '#babac2',
          200: '#92929e',
          300: '#777786',
          350: '#F4F4F4',
          400: '#5f5f6b',
          500: '#474750',
          600: '#393940',
          630: '#3E4150',
          650: '#23252B',
          700: '#28282E',
          800: '#191C25',
          900: '#13141f'
        },
        green: {
          DEFAULT: '#72F34B',
          50: '#F9FEF7',
          100: '#EAFDE4',
          200: '#CCFBBE',
          300: '#AEF898',
          400: '#90F671',
          500: '#72F34B',
          600: '#49EF16',
          700: '#6CDB00',
          800: '#288C09',
          900: '#195806'
        },
        yellow: {
          DEFAULT: '#FFB800',
          50: '#FFEBB8',
          100: '#FFE5A3',
          200: '#FFDA7A',
          300: '#FFCF52',
          400: '#FFC329',
          500: '#FFB800',
          600: '#C79000',
          700: '#8F6700',
          800: '#573F00',
          900: '#1F1600'
        },
        red: {
          DEFAULT: '#DE4343'
        },
        ...colors
      }
    }
  },
  plugins: []
}
