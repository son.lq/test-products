import { useState, useRef, useEffect } from 'react'
import Flicking from '@egjs/react-flicking'
import { Sync, AutoPlay } from '@egjs/flicking-plugins'
import '@egjs/flicking/dist/flicking.css'

const Carousel = ({ items }: { items: any[] }) => {
  const flicking0 = useRef()
  const flicking1 = useRef()

  const [plugins, setPlugins] = useState([])

  useEffect(() => {
    setPlugins([
      new Sync({
        type: 'index',
        synchronizedFlickingOptions: [
          {
            flicking: flicking0.current,
            isSlidable: true
          },
          {
            flicking: flicking1.current,
            isClickable: true,
            activeClass: 'border-gamefiGreen-500'
          }
        ]
      }),
      new AutoPlay({ duration: 10000, direction: 'NEXT', stopOnHover: true })
    ])
  }, [])

  return (
    <>
      <Flicking
        ref={flicking0}
        className={`mb-4 w-full`}
        bounce={5}
        plugins={plugins}
        preventClickOnDrag={false}
      >
        {items &&
          items.map((item, i) => (
            <img
              key={i}
              src={item}
              className="aspect-[16/9] w-full"
              alt="image"
            />
          ))}
      </Flicking>

      <Flicking
        ref={flicking1}
        moveType="freeScroll"
        bound={true}
        preventClickOnDrag={false}
        bounce={5}
      >
        {items &&
          items.map((item, i) => (
            <div
              key={i}
              style={{ borderColor: 'transparent' }}
              className="min-h-[90px] cursor-pointer rounded border-2 p-[2px]"
            >
              <div
                className="h-full w-40 rounded bg-cover"
                style={{ backgroundImage: `url(${item})` }}
              ></div>
            </div>
          ))}
      </Flicking>
    </>
  )
}

export default Carousel
