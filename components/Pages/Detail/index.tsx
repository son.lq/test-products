import React from 'react'
import Carousel from './Carousel'

export default function Detail({ data }) {
  const {
    images,
    brand,
    category,
    description,
    discountPercentage,
    price,
    rating,
    stock
  } = data
  return (
    <div className="grid grid-cols-3 gap-4">
      <div className="col-span-2">
        <Carousel items={images} />
      </div>
      <div className="">
        <div className="mb-5 text-xl font-medium">{description}</div>
        <div className=" mb-5">
          <span className="text-gray-500 line-through">
            {parseInt(String(price * discountPercentage))}$
          </span>{' '}
          <span className="text-xl font-semibold text-green-500">{price}$</span>{' '}
          <span className="text-sm text-red-600">
            (discount {discountPercentage}%)
          </span>
        </div>
        <div className="mb-3 flex justify-between">
          <div className="capitalize text-gray-300">rating</div>
          <div className="font-medium">{rating}</div>
        </div>
        <div className="mb-3 flex justify-between">
          <div className="capitalize text-gray-300">stock</div>
          <div className="font-medium">{stock}</div>
        </div>
        <div className="mb-3 flex justify-between">
          <div className="capitalize text-gray-300">brand</div>
          <div className="font-medium">{brand}</div>
        </div>
        <div className="mb-3 flex justify-between">
          <div className="capitalize text-gray-300">category</div>
          <div className="font-medium">{category}</div>
        </div>
      </div>
    </div>
  )
}
