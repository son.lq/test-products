import clsx from 'clsx'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import isEmpty from 'lodash.isempty'
import { debounce } from '@/utils'
import Loading from '@/components/Loading'

const DEBOUNCE_TIME_SEARCH = 1000

function TopFilter({ onSearch, searchValue }) {
  return (
    <>
      <div className="mb-[38px] flex">
        <div className="relative w-full text-sm sm:mr-auto sm:w-auto">
          <input
            defaultValue={searchValue}
            onKeyUp={onSearch}
            type="text"
            className="w-full rounded border-none border-dark-600 bg-[#242732] py-1.5 pl-8 leading-6 shadow-lg md:w-80"
            placeholder="Search"
          />
          <svg
            viewBox="0 0 13 14"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            className="absolute inset-y-0 left-2 top-2 mt-[3px] h-4 w-4"
          >
            <path
              d="M12.6092 12.1123L9.64744 9.03184C10.409 8.12657 10.8262 6.98755 10.8262 5.80178C10.8262 3.03135 8.57221 0.777344 5.80178 0.777344C3.03135 0.777344 0.777344 3.03135 0.777344 5.80178C0.777344 8.57222 3.03135 10.8262 5.80178 10.8262C6.84184 10.8262 7.83297 10.5125 8.68035 9.91702L11.6646 13.0208C11.7894 13.1504 11.9572 13.2218 12.1369 13.2218C12.3071 13.2218 12.4686 13.1569 12.5911 13.0389C12.8515 12.7884 12.8598 12.3729 12.6092 12.1123ZM5.80178 2.08807C7.84957 2.08807 9.5155 3.754 9.5155 5.80178C9.5155 7.84957 7.84957 9.5155 5.80178 9.5155C3.754 9.5155 2.08807 7.84957 2.08807 5.80178C2.08807 3.754 3.754 2.08807 5.80178 2.08807Z"
              fill="white"
            ></path>
          </svg>
        </div>
      </div>
    </>
  )
}

function Filter(props: {
  className?: any
  children?: any
  getData?: any
  pagination?: any
  loading?: any
  loadSkeleton?: any
}) {
  const { getData, pagination, loading, loadSkeleton } = props
  const { pageCount, page } = pagination || {}
  const router = useRouter()
  const [loadMore, setLoadMore] = useState(false)

  const [searchValue, setSearchValue] = useState<string>(
    (router.query.search as string) || ''
  )

  useEffect(() => {
    const search = router.query.search
    if (search) {
      setSearchValue(String(search))
    } else {
      setSearchValue('')
    }
  }, [router.query.search])

  useEffect(() => {
    const layoutElement = document.getElementById('layoutBody')
    layoutElement.addEventListener('scroll', handleLayoutScroll)

    return () => layoutElement.removeEventListener('scroll', handleLayoutScroll)
  }, [])

  useEffect(() => {
    if (page >= pageCount) return
    if (loadMore && !loading) {
      getData({ page: Number(page) + 1 })
    }
  }, [loadMore])

  useEffect(() => {
    if (!isEmpty(pagination)) {
      setLoadMore(false)
    }
  }, [pagination])

  const onSearch = debounce((event) => {
    const value = event.target.value.trim()
    const url = location.pathname
    if (value) {
      setSearchValue(value)
      router.replace(`${url}?search=${value}`)
    } else {
      setSearchValue('')
      router.replace(`${url}`)
    }
  }, DEBOUNCE_TIME_SEARCH)

  let delayTimerReview: string | number | NodeJS.Timeout

  const handleLayoutScroll = () => {
    clearTimeout(delayTimerReview)
    delayTimerReview = setTimeout(function () {
      const detectBottomElement = document.getElementById('detectBottom')
      const bounding = detectBottomElement?.getBoundingClientRect()
      if (!bounding) return
      if (
        bounding.top >= 0 &&
        bounding.left >= 0 &&
        bounding.right <=
          (window.innerWidth || document.documentElement.clientWidth) &&
        bounding.bottom <=
          (window.innerHeight || document.documentElement.clientHeight)
      ) {
        !loading && setLoadMore(true)
      }
    }, 300)
  }

  return (
    <div className={clsx('flex', props.className)}>
      <div className="flex-1 pl-[30px]">
        <TopFilter
          {...{
            searchValue,
            onSearch
          }}
        />
        {loadSkeleton ? (
          <div
            role="status"
            className="animate-pulse space-y-4  divide-y divide-gray-200 border-gray-200 p-4 shadow dark:divide-gray-700 dark:border-gray-700 md:p-6"
          >
            {Array.from(Array(10).keys()).map((v) => (
              <div className="flex items-center justify-between" key={v}>
                <div>
                  <div className="mb-2.5 h-4 w-24 rounded-full bg-gray-300 dark:bg-gray-600"></div>
                </div>
                <div className="h-2 w-32 rounded-full bg-gray-200 dark:bg-gray-700"></div>
                <div className="h-4 w-12 rounded-full bg-gray-300 dark:bg-gray-700 "></div>
              </div>
            ))}
            <span className="sr-only">Loading...</span>
          </div>
        ) : (
          <>
            {pageCount ? (
              <div className="">
                {props.children}
                <div className="relative">
                  <div id="detectBottom" className="h-[1px] w-full"></div>
                  {loading && <Loading />}
                </div>
              </div>
            ) : (
              <div className="py-4 text-center text-sm">
                There are no results to display
              </div>
            )}
          </>
        )}
      </div>
    </div>
  )
}

export default Filter
