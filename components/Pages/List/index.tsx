import clsx from 'clsx'
import { useRouter } from 'next/router'

export const HEADERS = [
  {
    text: '',
    className: ''
  },
  {
    text: 'Brand',
    className: ''
  },
  {
    text: 'title',
    className: 'px-1'
  },
  {
    text: 'category',
    className: 'px-1'
  },
  {
    text: 'price',
    className: ''
  },
  {
    text: 'rating',
    className: 'px-3'
  }
]

export default function List({ data }: any) {
  const router = useRouter()

  const goDetail = (id: string) => () => router.push(`/${id}`)

  return (
    <table className="w-full table-auto">
      <thead>
        <tr>
          {HEADERS.map((e, i) => {
            return (
              <th
                key={`header_${i}`}
                className={clsx(
                  'font-mechanic py-4 text-[13px] font-bold uppercase leading-[150%] tracking-[0.04em] text-white opacity-50',
                  e.className
                )}
              >
                <div className={clsx('flex w-fit items-center gap-1')}>
                  {e.text}
                </div>
              </th>
            )
          })}
        </tr>
      </thead>
      <tbody>
        {data.map((e) => {
          return (
            <tr
              key={e.id}
              onClick={goDetail(e.id)}
              className="cursor-pointer overflow-hidden rounded border-b-4 border-dark-900 bg-dark-650 hover:bg-dark-600"
            >
              <td>
                <div className="h-[103px] w-[200px] ">
                  <img
                    className="h-full w-full object-cover"
                    src={e.thumbnail}
                    alt="thumbnail"
                  />
                </div>
              </td>
              <td className="text-left text-sm font-normal leading-[100%] text-white">
                {e.brand}
              </td>
              <td>{e.title}</td>
              <td>{e.category}</td>
              <td>{e.price}</td>
              <td align="center">
                <div className="text-[15px] font-medium leading-[150%] text-white">
                  {e.rating}
                </div>
              </td>
            </tr>
          )
        })}
      </tbody>
    </table>
  )
}
