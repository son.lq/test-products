import React, { ReactNode } from 'react'
import Head from 'next/head'

const DEFAULT_TITLE = 'le son'
const DEFAULT_DESCRIPTION = 'test'

type Props = {
  children?: ReactNode
  title?: string
  url?: string
  description?: string
  image?: string
  className?: string
  keywords?: string
}

// const BETA_SUPPRESSION = 'BETA_SUPPRESSION'

const Layout = ({
  children,
  title,
  url,
  keywords,
  description,
  image,
  className = ''
}: Props) => {
  const theme = 'dark'

  return (
    <div className={`flex h-screen w-full ${theme}  font-inter`}>
      <div className="flex h-full w-full flex-col dark:bg-[#15181D] dark:text-white md:flex-row">
        <Head>
          <title>{title || DEFAULT_TITLE}</title>
          <meta charSet="utf-8" />
          <link rel="shortcut icon" href="/favicon.ico" />
          <meta
            name="viewport"
            content="initial-scale=1.0, width=device-width"
          />
          <meta
            property="og:title"
            content={title || DEFAULT_TITLE}
            key="title"
          />
          <meta
            name="description"
            content={description || DEFAULT_DESCRIPTION}
            key="desc"
          />
          <meta
            name="og:description"
            content={description || DEFAULT_DESCRIPTION}
            key="description"
          />
          <meta property="og:image" content={image} key="image" />
          <meta name="og:url" content={url || 'https://google.con'} />
          <meta name="keywords" content={keywords || 'test1, test2'}></meta>
          <meta name="twitter:card" content="summary_large_image" />
          <meta name="twitter:title" content={title || DEFAULT_TITLE} />
          <meta
            name="twitter:description"
            content={description || DEFAULT_DESCRIPTION}
          />
          <meta name="twitter:image" content={image} />
          <meta name="twitter:url" content={url || 'https://google.com'} />
          <meta name="twitter:site" content="@google" />
        </Head>
        <div
          id="layoutBody"
          className={`${className} relative h-full w-full overflow-auto scroll-smooth`}
        >
          {children}
        </div>
      </div>
    </div>
  )
}

export default Layout
