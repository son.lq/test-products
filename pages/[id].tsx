import { useEffect } from 'react'
import isEmpty from 'lodash.isempty'
import { useRouter } from 'next/router'
import Layout from '@/components/Layout'
import Detail from '@/components/Pages/Detail'
import Link from 'next/link'

const SingleProduct = ({ data }: any) => {
  const { id, title } = data
  const router = useRouter()

  useEffect(() => {
    if (isEmpty(data)) router.replace('/')
  }, [data])

  return (
    <Layout>
      <div className="container mx-auto mt-[100px] px-4 lg:px-24">
        <div className="mb-5 flex items-center gap-3">
          <div className=" text-2xl uppercase">{title}</div>
          {id > 1 && (
            <Link href={`/${id - 1}`} className="text-yellow-500">
              <svg
                width="8"
                height="8"
                viewBox="0 0 8 8"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <g clip-path="url(#clip0_9434_21208)">
                  <path
                    d="M5.75 0.25L2 4L5.75 7.75"
                    stroke="currentColor"
                    stroke-miterlimit="10"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                  />
                </g>
                <defs>
                  <clipPath id="clip0_9434_21208">
                    <rect width="8" height="8" fill="white" />
                  </clipPath>
                </defs>
              </svg>
            </Link>
          )}
          <Link href={`/${id + 1}`} className="text-yellow-500">
            <svg
              width="8"
              height="8"
              viewBox="0 0 8 8"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <g clip-path="url(#clip0_9434_21197)">
                <path
                  d="M2.25 0.25L6 4L2.25 7.75"
                  stroke="currentColor"
                  stroke-miterlimit="10"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                />
              </g>
              <defs>
                <clipPath id="clip0_9434_21197">
                  <rect width="8" height="8" fill="white" />
                </clipPath>
              </defs>
            </svg>
          </Link>
        </div>
        <Detail data={data} />
      </div>
    </Layout>
  )
}
export async function getStaticProps({ params }) {
  const id = params.id
  if (!id) {
    return { props: { data: {} } }
  }
  try {
    const data = await fetch(`https://dummyjson.com/products/${id}`).then(
      (res) => res.json()
    )
    if (isEmpty(data)) {
      return { props: { data: {} } }
    }

    return {
      props: {
        data
      }
    }
  } catch (e) {
    return { props: { data: {} } }
  }
}

export async function getStaticPaths () {
  const posts = await fetch(`https://dummyjson.com/products`).then(res => res.json())
  const paths = posts.products.map((post) => ({
    params: { id: post.id.toString() }
  }))
  
  return { paths, fallback: false }
}
export default SingleProduct
