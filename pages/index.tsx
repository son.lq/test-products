import { Inter } from '@next/font/google'
import Layout from '@/components/Layout'
import { useState, useEffect } from 'react'
import clsx from 'clsx'
import Filter from '@/components/Pages/List/Filter'
import List from '@/components/Pages/List'
import { useRouter } from 'next/router'

const inter = Inter({ subsets: ['latin'] })

const PAGE_SIZE = 10

export default function Home() {
  const router = useRouter()

  const [data, setData] = useState([])
  const [pagination, setPagination] = useState({})
  const [loadSkeleton, setLoadSkeleton] = useState(true)
  const [loading, setLoading] = useState(false)
  const [firstCome, setFirstCome] = useState(true)

  useEffect(() => {
    if (firstCome) {
      // React 18 - Avoiding Use Effect Getting Called Twice
      setFirstCome(false)
      setFirstCome(false)
      return
    }
    const search = String(router.query.search || '')
    if (getData) {
      getData({ page: 1, search })
    }
  }, [firstCome, router.query.search])

  const getData = ({ page, search }: { page: number; search?: string }) => {
    setLoading(true)
    const query = search || router.query.search
    const url = query
      ? `https://dummyjson.com/products/search?&q=${
          query || ''
        }&limit=${PAGE_SIZE}&skip=${(page - 1) * PAGE_SIZE}`
      : `https://dummyjson.com/products?limit=${PAGE_SIZE}&skip=${
          (page - 1) * PAGE_SIZE
        }`
    fetch(url)
      .then((res) => res.json())
      .then(({ products, total }: any) => {
        setPagination({ pageCount: total / PAGE_SIZE, page })
        if (products.length) {
          setData(page === 1 ? products : [...data, ...products])
        }
        setLoadSkeleton(false)
        setLoading(false)
      })
      .catch((err) => {
        setLoadSkeleton(false)
        setLoading(false)
        console.debug('err', err)
      })
  }
  return (
    <Layout>
      <div className={clsx(inter.className, 'mb-[100px] mt-9 w-full')}>
        <Filter
          className="mt-6 w-full px-4 md:mt-[60px] xl:px-12 2xl:px-[148px] 1"
          loading={loading}
          loadSkeleton={loadSkeleton}
          pagination={pagination}
          getData={getData}
        >
          <List data={data} pagination={pagination} />
        </Filter>
      </div>
    </Layout>
  )
}
