export const debounce = (fn: (arg0: any) => void, timer: number) => {
  let timeout: any
  return function (args?: any) {
    clearTimeout(timeout)
    timeout = setTimeout(() => {
      fn(args)
    }, timer)
  }
}
